from django.contrib import admin
from discography.models import Artist, Album, Label

admin.site.register(Artist)
admin.site.register(Album)
admin.site.register(Label)
