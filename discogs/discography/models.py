from django.db import models

class Artist(models.Model):
    name = models.CharField(max_length=255, verbose_name="Имя")
    surname = models.CharField(max_length=255, verbose_name="Фамилия")
    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Фото")
    
    def __str__(self):
        return f'{self.name} {self.surname}'

class Album(models.Model):
    album_name = models.CharField(max_length=255, verbose_name="Название Альбома")
    release_year = models.CharField(max_length=255, verbose_name="Год")
    cover = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Обложка")
    
    def __str__(self):
        return self.album_name
    
class Label(models.Model):
    label_name = models.CharField(max_length=255, verbose_name="Название Лэйбла")
    contact_info = models.TextField()
    sites = models.TextField()
    
    def __str__(self):
        return self.label_name